from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, reqparse, Api
from celery import Celery
import requests

app = Flask(__name__)
db = SQLAlchemy(app)
api = Api(app)

import os
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'tag.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# to start celery worker:
# $ celery -A tagAPI.celery worker --loglevel=info

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
celery = Celery('tagAPI', broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Integer)


@celery.task
def delete_tags(id):
    print("IN TASK")
    requests.put('http://127.0.0.1:5000', data={
        'id': str(id)
    })


class TagRoute(Resource):

    """deletes a tag. Must tell any other api using tag to also delete."""
    def delete(self):
        # first adds a tag to delete
        tag = Tag()
        db.session.add(tag)
        db.session.commit()

        parser = reqparse.RequestParser()
        parser.add_argument('id', type=int, required=True)
        parsed_args = parser.parse_args()
        id = parsed_args['id']

        tag = Tag.query.get(id)
        #if tag is not None:
            # Tag.query.get(id).delete()
            # db.session.commit()

        # delete tags from other API databases
        print("GIVING TASK")
        task = delete_tags.delay(id)


api.add_resource(TagRoute, '/tag')
db.create_all()

if __name__ == '__main__':
    app.run(port=5001, debug=True)
