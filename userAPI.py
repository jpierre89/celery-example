from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Resource, reqparse, Api

app = Flask(__name__)
db = SQLAlchemy(app)
api = Api(app)

import os
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'user.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


# 'tag' column exists in database only accessible by app2
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tag_id = db.Column(db.Integer, nullable=False)
    

class TagID(Resource):

    """deletes all tag with the id from all users with this tag"""
    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=int, required=True)
        parsed_args = parser.parse_args()
        id = parsed_args['id']

        users = User.query.filter(User.tag_id == id)
        for user in users:
            user.tag_id = None

        db.session.commit()

        print("RECEIVED CALL")


api.add_resource(TagID, '/tagid')
db.create_all()

if __name__ == '__main__':
    app.run(port=5000, debug=True)
